DESTDIR=/usr
PREFIX=/bin

OBJ=fang.o
EXE=fang

DEPS=list.h

LIBS=-lbluetooth -lpthread

all: $(EXE)

$(EXE): $(OBJ) $(DEPS)
	cc -o $(EXE) $(OBJ) $(LIBS)

install:
	mkdir -p $(DESTDIR)$(PREFIX)
	mv $(EXE) $(DESTDIR)$(PREFIX)

clean:
	rm -f $(EXE) $(OBJ) *~
